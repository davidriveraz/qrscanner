import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PantallaTextoPageRoutingModule } from './pantalla-texto-routing.module';

import { PantallaTextoPage } from './pantalla-texto.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PantallaTextoPageRoutingModule
  ],
  declarations: [PantallaTextoPage]
})
export class PantallaTextoPageModule {}
