import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-pantalla-texto',
  templateUrl: './pantalla-texto.page.html',
  styleUrls: ['./pantalla-texto.page.scss'],
})
export class PantallaTextoPage implements OnInit {
texto:any;
  constructor(private route:ActivatedRoute,
              private clipboard: Clipboard,
              public toastController: ToastController
    ) { }

  ngOnInit() {
    this.texto=this.route.snapshot.paramMap.get('texto');
    
    
  }
  copyClip(){
    this.clipboard.copy(this.texto);
    this.presentToast("texto copiado : "+this.texto);
  }
  async presentToast(texto) {
    const toast = await this.toastController.create({
      message: texto,
      duration: 2000
    });
    toast.present();
  }
}
