import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PantallaTextoPage } from './pantalla-texto.page';

describe('PantallaTextoPage', () => {
  let component: PantallaTextoPage;
  let fixture: ComponentFixture<PantallaTextoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PantallaTextoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PantallaTextoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
