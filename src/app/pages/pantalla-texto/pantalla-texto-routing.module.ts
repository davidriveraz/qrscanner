import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PantallaTextoPage } from './pantalla-texto.page';

const routes: Routes = [
  {
    path: '',
    component: PantallaTextoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PantallaTextoPageRoutingModule {}
