import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { AlertController, ToastController } from '@ionic/angular';
import { DataLocalService } from 'src/app/services/data-local.service';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(private dataLocal:DataLocalService,
    private barCodeScanner:BarcodeScanner,
    public toastController: ToastController,
    ) {}

    async scan(){
      this.barCodeScanner.scan().then(barcodeData => {
        if(!barcodeData.cancelled){
          //this.dataLocal.guardarRegistro(barcodeData.format,barcodeData.text);
         
         this.dataLocal.guardarLocalRemoto('QRCode',barcodeData.text);

        }

       }).catch(err => {
          
          this.dataLocal.guardarLocalRemoto('QRCode','312312312');
          //this.dataLocal.guardarRegistro('QRCode','https://www.google.com.pe/maps/@-15.6719996,-69.7789601,11z?hl=es-419');
           //this.dataLocal.guardarRegistro('QRCode','312312312');

       });    }

       async presentToast(texto) {
        const toast = await this.toastController.create({
          message: texto,
          duration: 2000
        });
        toast.present();
      }
}
