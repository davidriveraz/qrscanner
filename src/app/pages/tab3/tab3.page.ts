import { Component, OnInit } from '@angular/core';
import { DataLocalService } from 'src/app/services/data-local.service';

@Component({
  selector: 'app-tab3',
  templateUrl: './tab3.page.html',
  styleUrls: ['./tab3.page.scss'],
})
export class Tab3Page implements OnInit {

  constructor(private dataLocal:DataLocalService) { }

  ngOnInit() {

  }
  abrirRegistro(registro){
    this.dataLocal.abrirRegistro(registro);
  }
  eliminarLista(registroId){
  this.dataLocal.eliminarListaStorage(registroId);    
  }
  

}
