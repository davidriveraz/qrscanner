import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Registro } from '../modelos/registro.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FirebaseServiceService {
registro:AngularFirestoreCollection<Registro>;
registros:Observable<Registro[]>;
regi:AngularFirestoreDocument<Registro>;
registroObs:Observable<Registro>
  constructor(private afs:AngularFirestore) {
    this.registro=afs.collection('registro',ref=>ref.orderBy('created','asc'));
    this.registros=this.registro.snapshotChanges().pipe(
      map(action =>{
        return action.map(a=>{
          const data=a.payload.doc.data() as Registro;
          data.registroId=a.payload.doc.id;
          return data;
        })
      })
    );
   }

   getRegistros(){
    return this.registros;
   }
   getRegistro(registroId){
    this.regi=this.afs.doc(`registro/${registroId}`);
    return this.registroObs=this.regi.valueChanges();
  }
}
