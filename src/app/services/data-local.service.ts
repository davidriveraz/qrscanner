import { Injectable } from '@angular/core';
import { File } from '@ionic-native/file/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Registro } from '../modelos/registro.model';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { AngularFirestore } from '@angular/fire/firestore';
import { FirebaseServiceService } from './firebase-service.service';


@Injectable({
  providedIn: 'root'
})
export class DataLocalService {
  guardados:Registro[]=[];
  guardadosForStorage:Registro[]=[];
  constructor(private storage:Storage,
              private navCtrl:NavController,
              private inAppBrowser:InAppBrowser,
              private file:File,
              private emailComposer:EmailComposer,
              private afs:AngularFirestore,
              private toastr:ToastController,
              private firebaseServ:FirebaseServiceService,
              private alertCtrl:AlertController

              ) {

    this.cargarDeFirebase();
    this.cargarDeStorage();
   }

   async cargarDeFirebase(){
    this.firebaseServ.getRegistros().subscribe(registros=>{
      this.guardados=registros      
    })   
  }
  async cargarDeStorage() {
    this.guardadosForStorage = await this.storage.get('registros') || [];
  }

  async guardarRegistro(format:string,text:string){


    await this.cargarDeFirebase();
    const registroId=this.afs.createId();
    const nuevoRegistro=new Registro(format,text,registroId);
    this.afs.collection("registro").doc(registroId).set({
      'registroId':registroId,
      'format':nuevoRegistro.format,
      'text':nuevoRegistro.text,
      'type':nuevoRegistro.type,
      'icon':nuevoRegistro.icon,
      'created':nuevoRegistro.created
    })
    .then(()=>{
      this.presentToast('Escaneo guardado : '+nuevoRegistro.text,'success');            
    })
    .catch((err)=>{
      this.presentToast(err.message,'danger');
    }) 
    /* this.guardados.unshift(nuevoRegistro);
    console.log(this.guardados);
    this.storage.set('registros',this.guardados); */
    this.abrirRegistro(nuevoRegistro);

  }

  async guardarRegistroStorage(format:string,text:string){
    const registroId=this.afs.createId();

    const nuevoRegistro = new Registro( format, text,registroId );
    this.guardadosForStorage.unshift( nuevoRegistro );
    this.storage.set('registros', this.guardadosForStorage);
    this.abrirRegistro( nuevoRegistro );
  }


  abrirRegistro( registro: Registro ){
    this.navCtrl.navigateForward('/tabs/tab2');
    switch(registro.type){
      case 'http':
        this.inAppBrowser.create( registro.text,'_system');
        break;
      case 'geo':
        this.navCtrl.navigateForward(`/tabs/tab2/mapa/${registro.text}`);
        break;
      case 'indefinido':
        this.navCtrl.navigateForward(`/tabs/tab2/textos/${registro.text}`);
        break;


    }
  } 
  enviarCorreo(){
    const arrTemp = []
    const Titulos = "Tipo, Formato, Creado en, Texto\n";
    arrTemp.push(Titulos);
    this.guardados.forEach(registro => {
      const linea = `${ registro.type}, ${ registro.format}, ${ registro.created }, ${ registro.text.replace(',',' ')}\n`;
      arrTemp.push(linea);
    });

    console.log(arrTemp.join(''));
    
  }

  crearArchivoFisico(text:string)
  {
    this.file.checkFile(this.file.dataDirectory,'registro.csv')
    .then(existe =>{
      return this.escribirEnArchivo(text)
    })
    .catch(err=>{
      return this.file.createFile(this.file.dataDirectory,'registro.csv',false)
      .then(creado=>this.escribirEnArchivo(text))
      .catch(err2=> console.log("No se pudo crear archivo")
      )
    });
  }

  async escribirEnArchivo(text:string){
    await this.file.writeExistingFile(this.file.dataDirectory,'registro.csv',text);
    const archivo=`${this.file.dataDirectory}registro.csv`;
    
    const email = {
      to: 'davidzbuss@gmail.com',
      //cc: 'erika@mustermann.de',
      //bcc: ['john@doe.com', 'jane@doe.com'],
      attachments: [
        archivo
      ],
      subject: 'Archivos guardados',
      body: 'aqui esta el backup de scans',
      isHtml: true
    };
    this.emailComposer.open(email);
    
    
  }
  eliminarLista(registroId){
    this.afs.collection('registro').doc(registroId).delete()
    .then(()=>{
      
      this.presentToast('Eliminado correctamente','success');
      this.cargarDeFirebase();
    })
    .catch((err)=>{
      this.presentToast(err.message,'danger');
    })
  }

  eliminarListaStorage(registroId){
    for (var i =0; i < this.guardadosForStorage.length; i++){
      if (this.guardadosForStorage[i].registroId == registroId) {
         this.guardadosForStorage.splice(i);
      }
   } 
   this.storage.set('registros', this.guardadosForStorage);

  }

  async presentToast(msg,status) {
    const toast = await this.toastr.create({
      message: msg,
      position:'top',
      color:status,
      duration: 2000
    });
    toast.present();
  }



  //Para elegir en donde guardar
  async guardarLocalRemoto(format:string,text:string) {
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: 'Espera!',
      message: '¿Guardar escaneo en servidor remoto o local?',
      buttons: [
        {
          text: 'Remoto',
          cssClass: 'secondary',
          handler: (blah) => {
            this.guardarRegistro(format,text);
          }
        }, {
          text: 'Local',
          handler: () => {
            this.guardarRegistroStorage(format,text);
          }
        }
      ]
    });
    await alert.present();
  }

}
