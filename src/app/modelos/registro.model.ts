export class Registro{
    public registroId:string;
    public format:string;
    public text:string;
    public type:string;
    public icon:string;
    public created:string;

    constructor( format:string,text:string,registroId:string){
        this.format=format;
        this.text=text;
        this.created=String(Date.now());
        this.registroId=registroId;
        this.determarTipo();
    }

    private determarTipo(){

        const inicioTexto=this.text.substr(0,4);
        console.log('Tipo',inicioTexto);
        switch(inicioTexto){
            case 'http':
                this.type='http';
                this.icon="globe";
            break;
            case 'geo:':
                this.type='geo';
                this.icon="pin";
            break;  
            default:
                this.type='indefinido';
                this.icon='create'
        }
    }
}