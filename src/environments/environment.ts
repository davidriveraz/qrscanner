// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyC_n29YakH7bf7ztoEQdhb09mhur-6q4Rk",
    authDomain: "dbqrscanner.firebaseapp.com",
    projectId: "dbqrscanner",
    storageBucket: "dbqrscanner.appspot.com",
    messagingSenderId: "675863911814",
    appId: "1:675863911814:web:83306b43b7635de5c52007",
    measurementId: "G-P7CE63TKMC"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
